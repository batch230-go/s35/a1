
const express = require("express");
// Mongoose is a package that allows creation of Schemas to model our data structures
// Also has access to a number of methods for manipulating database
const mongoose = require("mongoose");


const app = express();
const port = 3001;

// [SECTION] MongoDB Connection
// Connect to the database by passing in your connection string, remember to replace the password and database names with actual values
/*
    Syntax:
        mongoose.connect("<MongoDB connection string>", {useNewUrlParser: true});
*/

mongoose.connect("mongodb+srv://admin:admin@batch230.pm3o66l.mongodb.net/S35?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// Connection to database
// Allows to handle errors when the initial connection is established
// Works with the on and once Mongoose methods
let db = mongoose.connection;

// If a connection error occured, output in console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal

db.on("error", console.error.bind(console, "connection error"));// << connection error
db.once("open", () => console.log(`We're connected to the cloud database`));// << connected

app.use(express.json());

const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: "pending"
    }
});
const Task = mongoose.model("Task", taskSchema);

app.post("/tasks", (request, response) => {
    // Check if there are duplicate task
    Task.findOne({name: request.body.name}, (error, result) => {
        // If there was found and the document's name matches the information via the client/Postman
        if(result != null && result.name == request.body.name){
            return response.send("Duplicate task found");
        }
        else{
            let newTask = new Task({
                name: request.body.name
            });
            newTask.save((saveError, savedTask) => {
                // if an error is saved in saveError parameter
                if(saveError){
                    return console.error(saveError);
                }
                else{
                    return response.status(201).send("New task created");
                }
            });
        }
    });
});

app.get("/tasks", (request, response) => {
    Task.find({}, (error, result) => {
        if(error){
            return console.log(error);
        }
        else{
            return response.status(200).json({
                data: result
            })
        }
    })
});



// Activity Part

//Schema Creation
const userSchema = new mongoose.Schema({
    username: String,
    password: String
});
const User = mongoose.model("User", userSchema);

// POST Part
app.post("/signup", (request, response) => {
    User.findOne({username: request.body.username}, (error, result) => {
        if(result != null && result.username == request.body.username){
            return response.send("Duplicate user found");
        }
        else{
            let newUser = new User({
                username: request.body.username,
                password: request.body.password
            });
            newUser.save((saveError, savedUser) => {
                if(saveError){
                    return console.error(saveError);
                }
                else{
                    return response.status(201).send("New user created");
                }
            });
        }
    });
});



app.listen(port, () => console.log(`Server running at port ${port}`));

